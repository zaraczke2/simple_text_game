import csv
from pathlib import Path
import os


class TextAdventureGame:
    def __init__(self):
        self.inventory_path = Path.cwd() / "inventory.csv"
        self.items_in_room = ["klucz", "lina", "drzwi"]
        self.lighter_flag = False
        self.key_exist_flag = False
        self.life = True
        self.inventory = self.read_csv_file(self.inventory_path)

    def read_csv_file(self, inventory_path):
        with inventory_path.open(mode="r", encoding="utf-8") as file:
            reader = csv.DictReader(file)
            items = [row for row in reader]
        return items

    def show_inventory(self):
        print("Oto zawartość ekwipunku:")
        for item in self.inventory:
            id = item["id"]
            name = item["name"]
            print(f"{id}. {name}")

    def take_an_item(self):
        if not self.lighter_flag:
            print("Nic nie widać jak chciałbyś coś wziąć?")
        else:
            for index, element in enumerate(self.items_in_room):
                print(f"{index + 1} - {element}")
            choice = int(input("Co chciałbyś wziąć? "))
            os.system("cls")
            if choice <= len(self.items_in_room):
                if choice == 1:
                    new_id = str(len(self.inventory) + 1)
                    self.inventory.append(
                        {'id': new_id, "name": self.items_in_room[choice - 1]})
                    del self.items_in_room[choice - 1]
                elif choice == 2:
                    print("Linę można przeciąć, ale nie zabrać.")
                elif choice == 3:
                    print("Drzwi możesz otworzyć kluczem, ale nie zabrać.")

    def use_of_items(self):
        decision = input("Chcesz któregoś użyć? T/N ")
        os.system("cls")
        if decision.lower() == "t":
            choice = int(input("Podaj numer przedmiotu: "))
            os.system("cls")
            if choice == 1:
                print("Użyłeś latarki, teraz możesz rozejrzeć się po pokoju")
                self.lighter_flag = not self.lighter_flag
            elif choice == 2:
                print("Przeciąłeś linę, uruchomiło to zapadnię, giniesz...")
                self.life = not self.life
            elif choice == 3:
                print("Użyłeś klucza, drzwi zostały otwarte, wygrałeś!")
                self.life = not self.life
            else:
                print("Niepoprawny wybór!")

    def show_command(self):
        print("Dostępne komendy:")
        print("opis - opisuje aktualne pomieszczenie")
        print("ekwipunek - wyświetla listę przedmiotów, które posiadasz")
        print("weź - zabiera przedmiot i umieszcza w inwentarzu")
        print("wyjście - zakończ grę")
        print()

    def player_choice(self, command):
        os.system("cls")
        if command == "opis":
            if self.lighter_flag:
                print("Po włączeniu latarki dostrzegasz drzwi, naprężoną linę i klucz.")
            else:
                print("W pomieszczeniu jest zupełnie ciemno, poszukaj jakiegoś światła. Co masz ze sobą?")
        elif command == "ekwipunek":
            self.show_inventory()
            self.use_of_items()
        elif command == "wez":
            self.take_an_item()
        elif command == "wyjscie":
            self.life = not self.life
        else:
            print("Niepoprawna komenda!")
    def start_game(self):
        while self.life:
            print()
            self.show_command()
            command = input("Co chcesz zrobić? ")
            self.player_choice(command)



game = TextAdventureGame()
game.start_game()
